import React from 'react'
import Navbar from './components/Navbar/Navbar'
import Hero from './components/Hero/Hero'
import Card from './components/Card/Card'
import data from './data'
import './App.css'

function App() {
  const cards = data.map(card => <Card key={card.id}  {...card} />)

  return (
    <div>
      <Navbar />
      <Hero />
      <section className="cards__list">
        {cards}
      </section>
    </div>
  );
}

export default App;
