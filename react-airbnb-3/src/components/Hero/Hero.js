import React from 'react'
import './Hero.css'


export default function Hero() {
    return (
        <section className="hero">
            <img
                src="../images/photo-grid.png"
                alt="grid_photo"
                className="hero__photo"
            />
                <h1 className="hero__title">Online Experiences</h1>
                <p className="hero__title_text">Join unique interactive activities led by
                    one-of-a-kind hosts—all without leaving home.
                </p>
        </section>
    )

}