import React from 'react'
import './Navbar.css'
import logoReact from "../../images/react-icon-small.png"

export default function Navbar() {
    return (
        <nav>
            <img
            src={logoReact}
            alt="React Logo"
            className='nav__logo'
            />
            <h3 className="nav__logo_text">ReactFacts</h3>
            <h4 className="nav__title">React - Project 1</h4>
        </nav>
    )
}